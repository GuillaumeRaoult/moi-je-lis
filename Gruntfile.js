module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        less  : {
            production: {
                options: {
                    strictMath: true
                },
                files  : [{
                    expand: true,
                    cwd   : 'wp-content/themes/moijelis/assets/less',
                    src   : [
                        'moijelis.less'
                    ],
                    dest  : 'wp-content/themes/moijelis/assets/css',
                    ext   : '.css'
                }]
            }
        },
        cssmin: {
            style: {
                src : 'wp-content/themes/moijelis/assets/css/moijelis.css',
                dest: 'wp-content/themes/moijelis/assets/css/moijelis.min.css'
            }
        },
        uglify: {
            tribe: {
                files: {
                    'wp-content/themes/moijelis/assets/js/moijelis.min.js': ['wp-content/themes/moijelis/assets/js/moijelis.js'],
                }
            }
        },
        watch : {
            scripts: {
                files: [
                    'wp-content/themes/moijelis/assets/js/moijelis.js'
                ],
                tasks: ['uglify:tribe']
            },
            styles : {
                files: [
                    'wp-content/themes/moijelis/assets/less/*.less',
                    'wp-content/themes/moijelis/assets/css/style.css'
                ],
                tasks: ['less', 'cssmin']
            }
        }
    });

    grunt.loadNpmTasks('grunt-css');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['less', 'cssmin', 'uglify:tribe']);

};