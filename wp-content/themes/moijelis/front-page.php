<?php

get_header();

?>
    <div class="row">
        <div class="content_top_home">
            <div class="container">
                <div class="row">
                    <div class="col-10 offset-1 col-lg-8 offset-lg-2 align-center">
                        <h1>
                            Une sélection de livres
                            <span>pour donner le goût de lire à votre enfant !</span>
                        </h1>
                    </div>
                </div>
                <?php
                get_template_part('template-parts/categories', 'listing');
                ?>
            </div>
        </div>
    </div>
    <div class="row content_bottom_home">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-10 offset-lg-1 d-none align-center products">
                    <h2>
                        Découvrez notre sélection
                        <span>Des livres adaptés au niveau de votre enfant, <br/>écrits par des auteurs jeunesse reconnus !</span>
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-10 offset-lg-1 align-center steps">
                    <div id="carouselFrontpageSteps" class="carousel slide">
                        <div class="row carousel-inner">
                            <div class="col-lg-4 col-12 active carousel-item step1">
                                <div class="illus">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/step1.png"/>
                                    01
                                </div>
                                <div class="desc">
                                    Je passe commande en ligne<br/> avant le 20 octobre et je bénéficie <span>de la livraison gratuite</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-12 carousel-item step2">
                                <div class="illus">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/step2.png"/>
                                    02
                                </div>
                                <div class="desc">
                                    Je suis livré gratuitement dans <br/>l’école de mon enfant à la rentrée
                                    <span>des vacances de la Toussaint</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-12 carousel-item step3">
                                <div class="illus">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/step3.png"/>
                                    03
                                </div>
                                <div class="desc">
                                    Mon enfant profite d’une sélection <br/>de livres adaptés à son niveau, et
                                    <span>développe son goût pour la lecture !</span>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-controls carousel-control-prev" href="#carouselFrontpageSteps"
                           role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-controls carousel-control-next" href="#carouselFrontpageSteps"
                           role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-10 offset-1 info_box">
                    <div class="row">
                        <div class="col-12 col-lg-6 text">Une sélection des Editions SEDRAP, <br/>éditeur scolaire de
                            référence de la <br/>maternelle et l’élémentaire.
                        </div>
                        <div class="col-12 col-lg-6 text">Des histoires attrayantes écrites par des auteurs jeunesse
                            reconnus
                            - Pour éveiller le plaisir de lire, et y prendre goût !
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

get_footer();