<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Moi_je_lis
 */

?>
</div>
<?php
if (!is_checkout()) {
    ?>
    <footer class="row align-center">
        <div class="company_infos">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3>
                            <span>En quelques mots</span>
                            Les Editions Sedrap
                        </h3>
                    </div>
                    <div class="col-12 col-lg-10 offset-lg-1">
                        <p>Editeur scolaire de référence depuis plus de 30 ans, nous sommes spécialistes de l’école
                            maternelle et élémentaire. Nous concevons des outils et manuels pédagogiques issus des
                            dernières
                            recherches en pédagogie et sciences de l’éducation, et conformes aux programmes scolaires
                            français en vigueur.</p>
                        <p>Nous éditons aussi une large choix d’albums, livres illustrés et romans, écrits par des
                            auteurs
                            jeunesse étudiés en classe, pour faciliter l’apprentissage de la lecture… et donner accès au
                            plaisir de lire.</p>
                        <p class="important">Chaque année, plus de 15000 écoles françaises nous font confiance !</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_menu">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-none d-lg-block">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'Footer',
                            'before'         => '<span class="sep">•</span>'
                        ));
                        ?>
                    </div>
                    <div class="col-12 d-lg-none">
                        <a href="javascript:void(0)" class="back_to_top">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/back-to-top.png"
                                 alt="Retour en haut"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="legal_infos">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php
                        $privacyPageId = get_option('wp_page_for_privacy_policy');
                        $cgvPageId     = get_option('woocommerce_terms_page_id');
                        ?>
                        <p>© Copyright 2018 <a href="http://www.sedrap.fr" target="_blank">sedrap.fr</a> • Tous les
                            droits
                            réservés | <a href="/<?php echo get_page_uri($cgvPageId) ?>">CGV et mentions légales</a> • <a href="/<?php echo get_page_uri($privacyPageId) ?>">Politique de confidentialité</a></p>
                        <p class="d-lg-block d-none">Edition SEDRAP • 9, Rue des Frères Boudé, 31106 Toulouse</p>
                        <div class="d-lg-none social">
                            <?php
                            get_template_part('template-parts/social', 'logos');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <?php
            get_template_part('template-parts/modal', 'addcart');
            do_action('moijelis_shop_closed_modal');
            ?>
        </div>
    </footer>
    <?php
}
?>
</div>
<?php wp_footer(); ?>
</body>
</html>
