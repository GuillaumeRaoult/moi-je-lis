<?php


class ShopOpen
{

    public function __construct()
    {
        add_filter('woocommerce_get_settings_general', array(
            $this,
            'createShopInactiveSettings'
        ), 10);

        if (!is_user_logged_in() && !isShopOpen()) {
            remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

            remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

            remove_action('woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20);

            remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);

            remove_action('woocommerce_checkout_after_customer_details', 'woocommerce_checkout_payment', 20);
        }

        add_action('template_redirect', array(
            $this,
            'redirectShopClosed'
        ), 10);

        add_action('woocommerce_before_main_content', array(
            $this,
            'showShopInactiveMessage'
        ), 5);

        add_action('woocommerce_before_cart', array(
            $this,
            'showShopInactiveMessage'
        ), 5);

        add_action('woocommerce_before_checkout_form', array(
            $this,
            'showShopInactiveMessage'
        ), 5);

        add_action('moijelis_shop_closed_modal', array(
            $this,
            'displayShopClosedModal'
        ));

    }

    public function redirectShopClosed()
    {
        if (!is_user_logged_in() && !isShopOpen() && (is_cart() || is_checkout() || is_checkout_pay_page())) {
            wp_safe_redirect('/', 301);
            exit;
        }
    }

    /**
     * Create options in Woo options page -> general
     *
     * @param $settings
     *
     * @return array
     */
    public function createShopInactiveSettings($settings)
    {
        $settings[] = array(
            'name' => __('Ouverture de la boutique', 'moijelis'),
            'type' => 'title',
            'id'   => 'shop_open'
        );
        $settings[] = array(
            'name' => __('Début ouverture', 'text-domain'),
            'id'   => 'shop_open_start',
            'type' => 'datetime-local',
        );
        $settings[] = array(
            'name' => __('Fin ouverture', 'text-domain'),
            'id'   => 'shop_open_end',
            'type' => 'datetime-local',
        );
        $settings[] = array(
            'name' => __('Message site fermé', 'text-domain'),
            'id'   => 'shop_open_closed_message',
            'type' => 'textarea',
        );
        $settings[] = array(
            'type' => 'sectionend',
            'id'   => 'shop_open'
        );

        return $settings;
    }

    public function displayShopClosedModal()
    {
        $cookie = isset($_COOKIE['wp-notice-site-expired']);
        if (!isShopOpen() && !$cookie) {
            get_template_part('template-parts/modal', 'shop-inactive');
        }
    }

    public function showShopInactiveMessage()
    {

        if (!isShopOpen()) {
            wc_print_notice(wpautop(get_option('shop_open_closed_message')), 'error');
        }
    }

}

add_action('woocommerce_init', 'initShopOpen');
function initShopOpen()
{
    $shopOpen = new ShopOpen();
}

function isShopOpen()
{
    date_default_timezone_set('Europe/Paris');

    $start = get_option('shop_open_start');
    $end   = get_option('shop_open_end');

    if (!$start) {
        $start = date('1950-12-12');
    }

    if (!$end) {
        $end = date('2050-12-12');
    }

    $start = strtotime($start);
    $end   = strtotime($end);

    if (time() >= $start && time() <= $end) {
        return true;
    }

    return false;
}