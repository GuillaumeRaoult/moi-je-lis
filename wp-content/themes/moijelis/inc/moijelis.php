<?php

class MoiJeLis
{
    public function __construct()
    {
        //Change breadcrumb delimiter
        add_filter('woocommerce_breadcrumb_defaults', array(
            $this,
            'changeBreadcrumbDelimiter'
        ), 10, 1);

        add_filter('body_class', array(
            $this,
            'bodyClass'
        ), 10, 2);

        //Post class
        add_filter('post_class', array(
            $this,
            'wcPostClass'
        ), 10, 3);

        //Remove links on tags
        add_filter('term_links-product_tag', array(
            $this,
            'removeTagLink'
        ), 10, 1);

        //Add class to add cart button
        add_filter('woocommerce_loop_add_to_cart_args', array(
            $this,
            'addClassesCartButton'
        ), 10, 2);

        //Add fragments
        add_filter('woocommerce_add_to_cart_fragments', array(
            $this,
            'addFragments'
        ), 10, 1);

        //Stop redirecting to product page and redirect to cat page
        add_filter('woocommerce_cart_redirect_after_error', array(
            $this,
            'changeRedirectAfterError'
        ), 10, 2);

        //Alter checkout fields
        add_filter('woocommerce_checkout_fields', array(
            $this,
            'changeCheckoutFields'
        ), 10, 1);

        //Add special classes in checkout form
        add_filter('woocommerce_form_field', array(
            $this,
            'changeFormFieldLayout'
        ), 10, 4);

        //Add text school recap after form
        add_action('woocommerce_checkout_after_customer_details', array(
            $this,
            'formSchoolRecap'
        ), 20);

        //Pay order label
        add_filter('woocommerce_order_button_text', array(
            $this,
            'changePayOrderLabel'
        ), 10, 1);

        //Transform school fields in shipping fields
        add_action('woocommerce_checkout_posted_data', array(
            $this,
            'saveShippingFields'
        ), 10, 1);

        //Save Tiers ID as meta
        add_action('woocommerce_checkout_update_order_meta', array(
            $this,
            'saveMetas'
        ), 10, 2);

        //Show TIERS ID meta in admin order shipping
        add_action('woocommerce_admin_order_data_after_shipping_address', array(
            $this,
            'adminAddInfos'
        ), 10, 1);

        //Add class to shipping address
        add_filter('woocommerce_order_formatted_shipping_address', array(
            $this,
            'modifyShippingAddress'
        ), 10, 2);

        //Change french address format to add class
        add_filter('woocommerce_localisation_address_formats', array(
            $this,
            'modifyAddressFormat'
        ), 10, 1);

        //Add the class replacement for variable
        add_filter('woocommerce_formatted_address_replacements', array(
            $this,
            'addClassReplacement'
        ), 10, 2);

        //Remove {class} is empty
        add_filter('trim_formatted_address_line', array(
            $this,
            'addClassReplacement'
        ), 10, 2);

        //Hide product link
        add_filter('woocommerce_order_item_permalink', array(
            $this,
            'hideProductLink'
        ), 10, 3);

        //Add T.T.C. after Total label
        add_filter('woocommerce_get_order_item_totals', array(
            $this,
            'changeTotalLabel'
        ), 10, 3);

        //Redirect single product page to cat
        add_filter('wp', array(
            $this,
            'redirectSingleProduct'
        ));

        //Remove the single imge size
        add_filter('wp_calculate_image_srcset_meta', array(
            $this,
            'removeSingleImageSize'
        ), 10, 4);

        //If failed order for admin, send same email for customer but change template and dest
        add_filter('wc_get_template', array(
            $this,
            'getTemplateFailedEmail'
        ), 10, 5);

        add_filter('gettext', array(
            $this,
            'changeTranslation'
        ), 10, 3);

        //Remove some useless loop template
        remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
        remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);

        //Remove checkout button from sidebar and add it under form
        remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);
        add_action('woocommerce_checkout_after_customer_details', 'woocommerce_checkout_payment', 20);

        //Remove "order again" button from order confirmation page sidebar
        remove_action('woocommerce_order_details_after_order_table', 'woocommerce_order_again_button');
    }

    public function changeBreadcrumbDelimiter($args)
    {
        $args['delimiter'] = '&nbsp;&gt;&nbsp;';

        return $args;
    }

    public function bodyClass($classes, $class)
    {
        if (!is_front_page() && !is_checkout() && !is_page_template('template-contact.php') && !is_tax()) {
            $classes[] = 'simple-page';
        }

        return $classes;
    }

    public function wcPostClass($classes, $class, $postID)
    {
        if (is_cart()) {
            $classes[] = 'col-12 col-lg-8 offset-lg-2';
        } else if (is_checkout()) {
            $classes[] = 'col-12 col-lg-7 offset-lg-1';
        } else {
            $classes[] = 'col-12 col-lg-10 offset-lg-1';
        }

        return $classes;
    }

    public function removeTagLink($links)
    {
        return array_map('wp_strip_all_tags', $links);
    }

    public function addClassesCartButton($args, $product)
    {
        $args['class'] .= ' btn orange';

        return $args;
    }

    function addFragments($fragments)
    {
        $fragments['nav#site-navigation .cart_icon'] = $this->getCartIconContent();
        $fragments['#modalCart']                     = $this->getCartModalContent();

        return $fragments;
    }

    public function changeRedirectAfterError($link, $productId)
    {
        $terms = get_the_terms($productId, 'product_cat');
        if ($terms) {
            $term = $terms[0];
            $link = get_term_link($term->term_id);
        }

        return $link;
    }

    public function changeCheckoutFields($fields)
    {
        unset($fields['billing']['billing_company']);
        unset($fields['billing']['billing_state']);
        unset($fields['billing']['billing_country']);
        unset($fields['billing']['billing_address_2']);
        unset($fields['order']['order_comments']);
        unset($fields['shipping']);

        $fields['billing']['billing_civility'] = array(
            'label'        => 'Civilité',
            'required'     => true,
            'type'         => 'select',
            'options'      => array(
                ''     => 'Civilité',
                'Mme'  => 'Mme',
                'Mlle' => 'Mlle',
                'M'    => 'M'
            ),
            'class'        => array(),
            'autocomplete' => 'civility',
            'priority'     => 10
        );

        $orderFields = array(
            'billing_civility'   => 'Civilité',
            'billing_last_name'  => 'Nom',
            'billing_first_name' => 'Prénom',
            'billing_email'      => 'Adresse email',
            'billing_phone'      => 'Téléphone',
            'billing_address_1'  => 'Adresse',
            'billing_postcode'   => 'Code postal',
            'billing_city'       => 'Ville'
        );
        $newFields   = array();
        $k           = 0;
        foreach ($orderFields as $lib => $placeholder) {
            $newFields['billing'][$lib]                  = $fields['billing'][$lib];
            $newFields['billing'][$lib]['priority']      = ($k * 10);
            $newFields['billing'][$lib]['input_class'][] = 'form-control';
            $newFields['billing'][$lib]['placeholder']   = $placeholder;
            $newFields['billing'][$lib]['required']      = true;

            unset($fields['billing'][$lib]);
            $k++;
        }

        $fields['shipping']['children_lastname'] = array(
            'label'        => 'Le nom de votre enfant',
            'required'     => true,
            'type'         => 'text',
            'placeholder'  => 'Nom de l\'enfant',
            'input_class'  => array('form-control'),
            'autocomplete' => 'family-name',
            'priority'     => 10
        );

        $fields['shipping']['children_firstname'] = array(
            'label'       => 'Le prénom de votre enfant',
            'required'    => true,
            'type'        => 'text',
            'placeholder' => 'Prénom de l\'enfant',
            'input_class' => array('form-control'),
            'priority'    => 20
        );

        $fields['shipping']['children_class'] = array(
            'label'       => 'La classe de votre enfant',
            'required'    => true,
            'type'        => 'select',
            'options'     => array(
                ''    => 'Classe',
                'TPS' => 'TPS',
                'PS'  => 'PS',
                'MS'  => 'MS',
                'GS'  => 'GS',
                'CP'  => 'CP',
                'CE1' => 'CE1',
                'CE2' => 'CE2',
                'CM1' => 'CM1',
                'CM2' => 'CM2'
            ),
            'input_class' => array('form-control'),
            'priority'    => 30
        );

        $fields['shipping']['children_schoolcity'] = array(
            'label'        => '',
            'required'     => false,
            'type'         => 'select',
            'placeholder'  => 'Ville',
            'options'      => array(
                '' => 'Ville',
            ),
            'input_class'  => array('form-control city'),
            'autocomplete' => 'address-level2',
            'priority'     => 40
        );

        $fields['shipping']['children_schoolname'] = array(
            'label'        => 'L\'école de votre enfant',
            'required'     => true,
            'type'         => 'select',
            'placeholder'  => 'Nom de l\'école',
            'options'      => array(
                '' => 'Nom de l\'école'
            ),
            'input_class'  => array('form-control'),
            'autocomplete' => 'address-level2',
            'priority'     => 50
        );

        $fields['shipping']['children_schoolcp'] = array(
            'label'        => '',
            'required'     => false,
            'type'         => 'select',
            'placeholder'  => 'Code postal',
            'options'      => array(
                '' => 'Code postal',
            ),
            'input_class'  => array('form-control'),
            'autocomplete' => 'postal-code',
            'priority'     => 60
        );

        $newFields = $newFields + $fields;

        return $newFields;
    }

    public function changeFormFieldLayout($field, $key, $args, $value)
    {
        if ($key == 'children_schoolcity') {
            $field = $field.' <p class="form_notice">Si votre ville commence par « SAINT- » : veuillez taper « ST-» pour la retrouver dans la liste.</p>';
        }

        $field = '<div class="col-12 col-lg-4"><div class="form-group">' . $field . '</div></div>';

        $rowOpen = array(
            'billing_civility',
            'billing_email',
            'billing_address_1',
            'children_lastname',
            'children_schoolcity'
        );
        if (in_array($key, $rowOpen)) {
            $field = '<div class="row">' . $field;
        }

        $rowClosed = array(
            'billing_first_name',
            'billing_phone',
            'billing_city',
            'children_class',
            'children_schoolcp'
        );
        if (in_array($key, $rowClosed)) {
            $field = $field . '</div>';
        }

        return $field;
    }

    public function formSchoolRecap()
    {
        ?>
        <div class="row">
            <div class="col-12 school_recap">
                <h4>Adresse de livraison</h4>
                <p class="name"></p>
                <p class="address"></p>
                <p class="city"><span class="cp"></span> <span class="ville"></span></p>
            </div>
        </div>
        <?php
    }

    public function changePayOrderLabel($label)
    {
        return 'payer ma commande';
    }

    public function saveShippingFields($posted_data)
    {
        $posted_data['billing_country']     = 'FR';
        $posted_data['shipping_first_name'] = $posted_data['children_firstname'];
        $posted_data['shipping_last_name']  = $posted_data['children_lastname'];
        $posted_data['shipping_class']      = $posted_data['children_class'];

        $schoolId = $posted_data['children_schoolname'];
        $school   = getSchoolById($schoolId);
        $address1 = $school->T_RAISON_SOCIALE . ' ' . $school->T_COMP_RAISON_SOCIALE;
        $address2 = $school->T_ADRESSE1;

        if ($school->T_ADRESSE2) {
            $address2 .= ' ' . $school->T_ADRESSE2;
        }
        if ($school->T_ADRESSE3) {
            $address2 .= ' ' . $school->T_ADRESSE3;
        }
        $posted_data['shipping_address_1'] = $address1;
        $posted_data['shipping_address_2'] = $address2;
        $posted_data['shipping_city']      = $school->T_VILLE;
        $posted_data['shipping_country']   = 'FR';
        $posted_data['shipping_postcode']  = $school->T_CODEPOSTAL;
        $posted_data['shipping_tiersid']   = $school->T_TIERS;

        return $posted_data;
    }

    public function saveMetas($order_id, $data)
    {
        update_post_meta($order_id, '_tiers_id', $data['shipping_tiersid']);
        update_post_meta($order_id, '_children_class', $data['shipping_class']);
    }

    public function adminAddInfos($order)
    {
        ?>
        <p>
            <strong>Ecole Tiers ID :</strong>
            <?php echo get_post_meta($order->get_id(), '_tiers_id', true) ?>
        </p><br/>
        <?php
    }

    public function modifyShippingAddress($address, \WC_Order $WC_Order)
    {
        $address['class'] = get_post_meta($WC_Order->get_id(), '_children_class', true);

        return $address;
    }

    public function modifyAddressFormat($formats)
    {
        $formats['FR'] = "{company}\n{name}\n{class}\n{address_1}\n{address_2}\n{postcode} {city_upper}\n{country}";

        return $formats;
    }

    public function addClassReplacement($rep, $args)
    {
        $rep['{class}'] = '';
        if (isset($args['class'])) {
            $rep['{class}'] = 'Classe ' . $args['class'];
        }

        return $rep;
    }

    public function hideProductLink($link, $item, $order)
    {
        return '';
    }

    public function changeTotalLabel($totalRows, \WC_Order $orderClass, $taxDisplay)
    {
        foreach ($totalRows as $lab => $totArray) {
            if ($lab == 'order_total') {
                $totalRows[$lab]['label'] = 'Total&nbsp;T.T.C.&nbsp;:';
            }
        }

        return $totalRows;
    }

    public function redirectSingleProduct()
    {
        if (is_product()) {
            global $post;
            $catIds = wc_get_product_cat_ids($post->ID);
            foreach ($catIds as $catId) {
                $catUrl = get_term_link($catId);
                wp_redirect($catUrl);
                exit;
            }
        }
    }

    public function removeSingleImageSize($image_meta, $size_array, $image_src, $attachment_id)
    {
        if (isset($image_meta['sizes']['woocommerce_single'])) {
            unset($image_meta['sizes']['woocommerce_single']);
        }

        return $image_meta;
    }

    public function getTemplateFailedEmail($located, $template_name, $args, $template_path, $default_path)
    {
        if ($template_name != 'emails/admin-failed-order.php') {
            return $located;
        }

        //Si admin email == client email
        if ($args['order']->get_billing_email() == $args['email']->get_option('recipient', get_option('admin_email'))) {
            return $located;
        }

        $recipient = $args['email']->get_recipient();
        if ($recipient == $args['email']->get_option('recipient', get_option('admin_email'))) {
            add_filter('woocommerce_email_recipient_failed_order', array(
                $this,
                'failedOrderCustomerEmail'
            ), 10, 2);
            $args['email']->trigger($args['order']->get_id(), null);
            remove_filter('woocommerce_email_recipient_failed_order', array(
                $this,
                'failedOrderCustomerEmail'
            ));
        } else {
            $located = wc_locate_template('emails/customer-failed-order.php', $template_path, $default_path);
        }

        return $located;
    }

    public function failedOrderCustomerEmail($recipient, $order)
    {
        return $order->get_billing_email();
    }

    public function changeTranslation($translation, $text, $domain)
    {
        if ($text == 'Please log in to your account below to continue to the payment form.') {
            $translation = 'Votre commande a expiré, merci de bien vouloir la recommencer';
        }

        return $translation;
    }

    /**
     * Return cart icon content in header
     * Also used by fragment
     *
     * @return string
     */
    private function getCartIconContent()
    {
        ob_start();
        global $woocommerce;
        $cartItemsCount = $woocommerce->cart->cart_contents_count;
        ?>
        <div class="col-lg-1 col-3 align-right cart_icon">
            <a href="<?php echo wc_get_cart_url() ?>">
                <?php
                $image = $cartItemsCount == 0 ? 'cart-empty' : 'cart-full';
                ?>
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $image ?>.png"
                     srcset="<?php echo get_template_directory_uri() ?>/assets/images/<?php echo $image ?>@2x.png 2x,
                            <?php echo get_template_directory_uri() ?>/assets/images/<?php echo $image ?>@3x.png 3x"
                     alt="Panier"/>
                <?php
                if ($cartItemsCount > 0) {
                    ?>
                    <span class="cart_count"><?php echo $cartItemsCount ?></span>
                    <?php
                }
                ?>
            </a>
        </div>
        <?php
        $cart_icon = ob_get_clean();

        return $cart_icon;
    }

    /**
     * Fragment for modal add to cart
     *
     * @return string
     */
    private function getCartModalContent()
    {
        ob_start();
        wc_get_template('template-parts/modal-addcart.php');
        $modal = ob_get_clean();

        return $modal;
    }

}


add_action('woocommerce_init', 'initMoiJeLis');
function initMoiJeLis()
{
    $moijelis = new MoiJeLis();
}

