<?php

add_action('cmb2_admin_init', 'moijelisCreatePosttypes');
function moijelisCreatePosttypes()
{
    register_post_type('question', // Register Custom Post Type
        array(
            'labels'             => array(
                'name'               => __('Questions', 'minisite'),
                'singular_name'      => __('Question', 'minisite'),
                'add_new'            => __('Ajouter', 'minisite'),
                'add_new_item'       => __('Ajouter une nouvelle question', 'minisite'),
                'edit'               => __('Modifier', 'minisite'),
                'edit_item'          => __('Modifier une question', 'minisite'),
                'new_item'           => __('Nouvelle question', 'minisite'),
                'view'               => __('Voir la question', 'minisite'),
                'view_item'          => __('Voir la question', 'minisite'),
                'search_items'       => __('Chercher une question', 'minisite'),
                'not_found'          => __('Aucune question trouvée', 'minisite'),
                'not_found_in_trash' => __('Aucune question dans la corbeille', 'minisite')
            ),
            'public'             => true,
            'publicly_queryable' => false,
            'hierarchical'       => true,
            'has_archive'        => false,
            'supports'           => array(
                'title',
                'editor',
                'page-attributes',
            ),
        ));
}
