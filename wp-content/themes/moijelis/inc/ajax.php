<?php

add_action('wp_ajax_nopriv_search_children_schoolcity', 'searchSchoolCity');
add_action('wp_ajax_search_children_schoolcity', 'searchSchoolCity');
function searchSchoolCity()
{
    $results = array('results' => array());

    $search = $_GET['search'];
    $term   = $search['term'];
    $page   = $_GET['page'];

    global $wpdb;
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM {$wpdb->prefix}ecoles ";
    $sql .= "WHERE T_VILLE LIKE '$term%' ";

    if ($search['cp']) {
        $cp  = $search['cp'];
        $sql .= "AND T_CODEPOSTAL = '$cp' ";
    }

    $sql .= "GROUP BY T_VILLE LIMIT 20 ";

    if ($page > 1) {
        $offset = ($page - 1) * 20;
        $sql    .= "OFFSET $offset ";
    }

    $queryResults = $wpdb->get_results($sql, OBJECT);
    if (!$queryResults) {
        echo json_encode($results);
        wp_die();
    }

    foreach ($queryResults as $k => $result) {
        $results['results'][] = array(
            'id'   => $result->T_VILLE,
            'text' => $result->T_VILLE
        );
    }

    $results['pagination']['more'] = false;
    if ($wpdb->get_var('SELECT FOUND_ROWS()') > 20) {
        $results['pagination']['more'] = true;
    }

    echo json_encode($results);
    wp_die();
}

add_action('wp_ajax_nopriv_search_children_schoolcp', 'searchSchoolCp');
add_action('wp_ajax_search_children_schoolcp', 'searchSchoolCp');
function searchSchoolCp()
{
    $results = array('results' => array());

    $search = $_GET['search'];
    $term   = $search['term'];
    $page   = $_GET['page'];

    global $wpdb;
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM {$wpdb->prefix}ecoles ";
    $sql .= "WHERE T_CODEPOSTAL LIKE '%$term%' ";

    if ($search['ville']) {
        $ville = $search['ville'];
        $sql   .= "AND T_VILLE = '$ville' ";
    }

    $sql .= "GROUP BY T_CODEPOSTAL ";
    $sql .= "LIMIT 20 ";

    if ($page > 1) {
        $offset = ($page - 1) * 20;
        $sql    .= "OFFSET $offset ";
    }

    $queryResults = $wpdb->get_results($sql, OBJECT);

    if (!$queryResults) {
        echo json_encode($results);
        wp_die();
    }

    foreach ($queryResults as $k => $result) {
        $results['results'][] = array(
            'id'   => $result->T_CODEPOSTAL,
            'text' => $result->T_CODEPOSTAL
        );
    }

    $results['pagination']['more'] = false;
    if ($wpdb->get_var('SELECT FOUND_ROWS()') > 20) {
        $results['pagination']['more'] = true;
    }

    echo json_encode($results);
    wp_die();
}

add_action('wp_ajax_nopriv_search_children_schoolname', 'searchSchoolName');
add_action('wp_ajax_search_children_schoolname', 'searchSchoolName');
function searchSchoolName()
{
    $results = array('results' => array());

    $search = $_GET['search'];
    $term   = $search['term'];
    $page   = $_GET['page'];

    global $wpdb;
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM {$wpdb->prefix}ecoles ";
    $sql .= "WHERE T_COMP_RAISON_SOCIALE LIKE '%$term%' ";

    if ($search['ville'] != '') {
        $ville = $search['ville'];
        $sql   .= "AND T_VILLE = '$ville' ";
    }

    if ($search['cp'] != '') {
        $ville = $search['cp'];
        $sql   .= "AND T_CODEPOSTAL = '$ville' ";
    }

    $sql .= "LIMIT 20 ";

    if ($page > 1) {
        $offset = ($page - 1) * 20;
        $sql    .= "OFFSET $offset ";
    }

    $queryResults = $wpdb->get_results($sql, OBJECT);

    if (!$queryResults) {
        echo json_encode($results);
        wp_die();
    }

    foreach ($queryResults as $k => $result) {
        $results['results'][] = array(
            'id'   => $result->T_TIERS,
            'text' => $result->T_RAISON_SOCIALE . ' ' . $result->T_COMP_RAISON_SOCIALE
        );
    }

    $results['pagination']['more'] = false;
    if ($wpdb->get_var('SELECT FOUND_ROWS()') > 20) {
        $results['pagination']['more'] = true;
    }

    echo json_encode($results);
    wp_die();
}

add_action('wp_ajax_nopriv_get_data_by_children_schoolcity', 'getDataByCity');
add_action('wp_ajax_get_data_by_children_schoolcity', 'getDataByCity');
function getDataByCity()
{
    $search = $_GET['search'];
    global $wpdb;
    $queryResults = $wpdb->get_results("SELECT T_CODEPOSTAL FROM {$wpdb->prefix}ecoles WHERE T_VILLE = '$search' GROUP BY T_CODEPOSTAL", OBJECT);
    $res          = array();
    foreach ($queryResults as $result) {
        $res['cp'][$result->T_CODEPOSTAL] = array(
            'id'   => $result->T_CODEPOSTAL,
            'text' => $result->T_CODEPOSTAL,
        );
    }
    echo json_encode($res);
    wp_die();
}

add_action('wp_ajax_nopriv_get_data_by_children_schoolcp', 'getDataByCP');
add_action('wp_ajax_get_data_by_children_schoolcp', 'getDataByCP');
function getDataByCP()
{
    $search = $_GET['search'];
    global $wpdb;

    //Fetch cities for this cp
    $queryResults = $wpdb->get_results("SELECT T_VILLE FROM {$wpdb->prefix}ecoles WHERE T_CODEPOSTAL = '$search' GROUP BY T_VILLE", OBJECT);
    foreach ($queryResults as $result) {
        $res['city'][$result->T_VILLE] = array(
            'id'   => $result->T_VILLE,
            'text' => $result->T_VILLE,
        );
    }

    //Fetch names for this cp
    $queryResults = $wpdb->get_results("SELECT T_RAISON_SOCIALE, T_COMP_RAISON_SOCIALE FROM {$wpdb->prefix}ecoles WHERE T_CODEPOSTAL = '$search'", OBJECT);
    $res          = array();
    foreach ($queryResults as $result) {
        $res['name'][$result->T_RAISON_SOCIALE . ' ' . $result->T_COMP_RAISON_SOCIALE] = array(
            'id'   => $result->T_TIERS,
            'text' => $result->T_COMP_RAISON_SOCIALE,
        );
    }

    echo json_encode($res);
    wp_die();
}

add_action('wp_ajax_nopriv_get_data_by_children_schoolname', 'getDataById');
add_action('wp_ajax_get_data_by_children_schoolname', 'getDataById');
function getDataById()
{
    $result  = getSchoolById($_GET['search']);
    $address = $result->T_ADRESSE1;
    if ($result->T_ADRESSE2) {
        $address .= ' ' . $result->T_ADRESSE2;
    }
    if ($result->T_ADRESSE3) {
        $address .= ' ' . $result->T_ADRESSE3;
    }
    $res = array(
        'name'    => $result->T_RAISON_SOCIALE . ' ' . $result->T_COMP_RAISON_SOCIALE,
        'address' => $address,
        'cp'      => $result->T_CODEPOSTAL,
        'ville'   => $result->T_VILLE,
    );
    echo json_encode($res);
    wp_die();
}

function getSchoolById($id)
{
    global $wpdb;
    $queryResults = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ecoles WHERE T_TIERS = '$id'", OBJECT);

    return $queryResults[0];
}