<?php

add_action('cmb2_admin_init', 'moijelis_register_taxonomy_metabox');
function moijelis_register_taxonomy_metabox()
{
    $prefix = 'moijelis_term_';
    /**
     * Metabox to add fields to categories and tags
     */
    $cmb_term = new_cmb2_box(array(
        'id'               => $prefix . 'edit',
        'title'            => esc_html__('Category Metabox', 'cmb2'),
        // Doesn't output for term boxes
        'object_types'     => array('term'),
        'taxonomies'       => array('product_cat'),
        'new_term_section' => true,
    ));
    $cmb_term->add_field(array(
        'name' => esc_html__('Titre page catégorie', 'cmb2'),
        'desc' => esc_html__('Titre de la catégorie sur la page dédiée', 'cmb2'),
        'id'   => $prefix . 'category_page_title',
        'type' => 'text',
    ));
    $cmb_term->add_field(array(
        'name' => esc_html__('Description page catégorie', 'cmb2'),
        'desc' => esc_html__('Description de la catégorie sur la page dédiée', 'cmb2'),
        'id'   => $prefix . 'category_page_description',
        'type' => 'textarea_small',
    ));
    $cmb_term->add_field(array(
        'name' => esc_html__('Description page catégorie bas', 'cmb2'),
        'desc' => esc_html__('Description de la catégorie sur la page dédiée en bas', 'cmb2'),
        'id'   => $prefix . 'category_page_description_bottom',
        'type' => 'textarea_small',
    ));
}

