(function ($) {

    moijelis = {
        init                 : function () {
            var self = this;
            self.initDom();
        },
        initDom              : function () {
            var _self = this;

            //Change quantity buttons on cat page and cart page
            $('body').on('click', '.quantity .quantity_change', function () {
                var val       = $(this).attr('data-change');
                var productId = $(this).attr('data-product-id');
                _self.changeQuantityField(val, productId);
                return false;
            });

            //Home slider
            if ($(window).width() <= 768) {
                $('#carouselFrontpageSteps').carousel();
                $('#carouselFrontpageSteps').swipe({
                    swipe          : function (event, direction, distance, duration, fingerCount, fingerData) {
                        if (direction == 'left') $(this).carousel('next');
                        if (direction == 'right') $(this).carousel('prev');
                    },
                    allowPageScroll: "vertical"
                });
            }

            //Mobile menu toggle
            $('.menu-toggle').click(function () {
                var open = $('#menu-header').hasClass('open');
                var top  = $('.menu_cont').offset().top - $(window).scrollTop();
                $('#menu-header').css('top', top + 'px');
                if (open) {
                    $(this).find('.dashicons').removeClass('dashicons-no-alt').addClass('dashicons-menu');
                    $('body').css('overflow', 'auto');
                } else {
                    $(this).find('.dashicons').removeClass('dashicons-menu').addClass('dashicons-no-alt');
                    $('body').css('overflow', 'hidden');
                }
                $('#menu-header').toggleClass('open');
            });

            var tooltipOptions = {
                'offset': '0 10'
            };
            if ($(window).width() > 1024) {
                tooltipOptions = {
                    'placement': 'right',
                    'offset'   : '0 10'
                };
            }
            $('[data-toggle="tooltip"]').tooltip(tooltipOptions);

            //Select contact
            $('#contact_reason').selectWoo({
                minimumResultsForSearch: Infinity
            });

            //Validation style
            if ($('#contact_reason').hasClass('is-valid')) {
                $('#contact_reason').next('.select2-container').find('.select2-selection').css('border-color', '#28a745');
            } else if ($('#contact_reason').hasClass('is-invalid')) {
                $('#contact_reason').next('.select2-container').find('.select2-selection').css('border-color', '#dc3545');
            }

            //Bkac to top button
            $('.back_to_top').click(function () {
                $("html, body").animate({scrollTop: 0}, 300);
                return false;
            });

            _self.showAddCartModal();
            _self.showShopInactiveModal();
            _self.checkoutSelects();
        },
        changeQuantityField  : function (change, productId) {
            //Display span
            var qteNumber = $('span.quantity_number[data-product-id="' + productId + '"]');
            var changeQte = parseInt(qteNumber.html());
            changeQte += parseInt(change);

            //Max qte
            if (changeQte > parseInt(qteNumber.data('max'))) {
                return false;
            }

            if (changeQte < 1) {
                return false;
            }

            qteNumber.html(changeQte);

            //Quantity field
            var qteField = $('input.qty[data-product-id="' + productId + '"]');
            qteField.val(changeQte);
            qteField.trigger('change');

            //Add to cart number of cat page
            if ($('body.tax-product_cat').length > 0) {
                var addToCartButton = $('.add_to_cart_button[data-product_id="' + productId + '"]');
                addToCartButton.data("quantity", changeQte);
            }

            if ($('body.woocommerce-cart').length > 0) {
                $('button[name="update_cart"]').click();
            }
            return false;
        },
        showAddCartModal     : function () {
            $('body').on('added_to_cart', function () {
                $('#modalCart').modal();
            });
        },
        showShopInactiveModal: function () {
            $('#modalShopInactive').modal();
            $('#modalShopInactive').on('hide.bs.modal', function (e) {
                var days = days;
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires     = "; expires=" + date.toUTCString();
                document.cookie = 'wp-notice-site-expired' + "=" + "false" + expires + "; path=/";
            });
        },
        checkoutSelects      : function () {
            //Regular select2, no search
            $('#billing_civility, #children_class').selectWoo({
                minimumResultsForSearch: Infinity
            });

            $('#clear_selection').click(function () {
                $("#children_schoolcp").val('').trigger('change');
                $("#children_schoolcity").val('').trigger('change');
                $("#children_schoolname").val('').trigger('change');
                $('.school_recap').hide();
            });

            $('#children_schoolcity, #children_schoolcp, #children_schoolname').each(function () {
                var selectId       = $(this).attr('id');
                var select2Options = {
                    minimumInputLength: 0,
                    language          : "fr",
                    allowClear        : true,
                    cache             : true,
                    ajax              : {
                        url     : wpscript.ajaxurl,
                        dataType: 'json',
                        data    : function (params) {
                            return {
                                action: 'search_' + $(this).attr('id'),
                                search: {
                                    'term' : params.term,
                                    'cp'   : $('#children_schoolcp').val(),
                                    'ville': $('#children_schoolcity').val(),
                                    'name' : $('#children_schoolname').val()
                                },
                                page  : params.page || 1
                            };
                        }
                    }
                };

                $(this).selectWoo(select2Options);

                //Select option
                $(this).on('select2:select', function (e) {
                    $.ajax({
                        type    : 'GET',
                        dataType: "json",
                        url     : wpscript.ajaxurl,
                        data    : {
                            search: e.params.data.id,
                            action: 'get_data_by_' + selectId
                        }
                    }).then(function (data) {
                        if (selectId == 'children_schoolname') {
                            //Set other selects
                            var newOptionVille = new Option(data.ville, data.ville, true, true);
                            $('#children_schoolcity').append(newOptionVille).trigger('change');

                            var newOptionCP = new Option(data.cp, data.cp, true, true);
                            $('#children_schoolcp').append(newOptionCP).trigger('change');

                            //populate school recap and ID field
                            $('.school_recap .name').html(data.name);
                            $('.school_recap .address').html(data.address);
                            $('.school_recap .city .cp').html(data.cp);
                            $('.school_recap .city .ville').html(data.ville);
                            $('.school_recap').show();
                        } else {
                            //Reset school name
                            $("#children_schoolname").val('').trigger('change');
                            $.each(data, function (type, dataset) {
                                var k = 1;
                                $.each(dataset, function (i, set) {
                                    //Select first option or already selected option if we have
                                    var selected = false;
                                    if (selectId == 'children_schoolcp') {
                                        var val = $('#children_schoolcity').val();
                                        if (val) {
                                            selected = false;
                                            if (set.id == val) {
                                                selected = true;
                                            }
                                        }
                                    }

                                    var newOption = new Option(set.id, set.id, selected, selected);

                                    //Select cp, fetch city and name
                                    if (type == 'city') {
                                        $('#children_schoolcity').append(newOption).trigger('change');
                                    } else if (type == 'cp') {
                                        $('#children_schoolcp').append(newOption).trigger('change');
                                    } else if (type == 'name') {
                                        $('#children_schoolname').append(newOption).trigger('change');
                                    }
                                    k++;
                                });
                            });
                        }

                    });
                });
            });
        }
    };

    $(document).ready(function () {
        moijelis.init();
    });

})(jQuery);