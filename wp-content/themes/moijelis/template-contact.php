<?php /* Template Name: Contact */ ?>

<?php

get_header();

$return = array();
if (isset($_POST["contact"]) && $_POST["contact"]) {
    $return = array(
        'error'  => true,
        'errors' => array('Une erreur inconnue est survenue, merci de réessayer plus tard.')
    );

    $required = array(
        'nom'     => 'Veuillez entrer un nom',
        'prenom'  => 'Veuillez entrer un prénom',
        'email'   => 'Veuillez entrer un email',
        'reason'  => 'Veuillez entrer un objet',
        'message' => 'Veuillez entrer un message',
    );

    $data = array();
    foreach ($_POST['contact'] as $field => $value) {
        $data[$field] = sanitize_text_field($value);
    }

    $errors = array();
    foreach ($required as $required_field => $message) {
        if (!isset($data[$required_field]) || $data[$required_field] == '') {
            $errors[$required_field] = $message;
        }
    }

    if (!isset($data["reason"]) || !$data["reason"] || $data["reason"] == 'null') {
        $errors['reason'] = $required['reason'];
    }

    if (!$errors) {
        $subject = 'Nouvelle demande de contact ' . get_bloginfo('name');
        $message = '
                <p>Bonjour,</p>
                <p>Une nouvelle demande de contact vient d’être envoyée sur le minisite ' . get_bloginfo('name') . ' :</p>
                <ul>
                    <li>Nom : ' . $data["nom"] . '</li>
                    <li>Prénom : ' . $data["prenom"] . '</li>
                    <li>Email : ' . $data["email"] . '</li>
                    <li>Objet : ' . $data["reason"] . '</li>
                    <li>Message : ' . $data["message"] . '</li>
                </ul>
            ';

        if (WP_DEBUG) {
            $to = 'ergeais@gmail.com';
        } else {
            $to = 'info@sedrap.fr';
        }

        $headers[] = 'From: Moijelis Sedrap<moijelis@sedrap.fr>';
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $sent = wp_mail($to, $subject, $message, $headers);

        if ($sent) {
            $return = array(
                'error'   => false,
                'success' => 'Votre message a été envoyé avec succès'
            );
        } else {
            $sent = wp_mail('g.raoult@gmail.com', 'Erreur lors de l\'envoi mail ' . get_bloginfo('name'), $message, $headers);
        }
    } else {
        $return = array(
            'error'  => true,
            'errors' => $errors
        );
    }
}

?>

<div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <header class="page-header align-center">
                        Informations &amp; support
                        <h1 class="page-title">
                            Une question ? Une remarque ?
                            <span>Utilisez le formulaire ci-dessous pour nous contacter, nous vous répondrons dans les
                                meilleurs délais.</span>
                        </h1>
                    </header>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-3 offset-lg-1 infos">
                    <a href="http://www.sedrap.fr" target="_blank">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo_sedrap.png" alt="Sedrap"
                             class="img-responsive"/>
                    </a>
                    <div class="adresse">
                        <p class="black">Adresse</p>
                        <p>SEDRAP</p>
                        <p>9, Rue des Frères Boudé</p>
                        <p>31106 Toulouse</p>
                    </div>
                    <div class="telephone">
                        <p class="black">Téléphone</p>
                        <p><a href="tel:+33561436243"><b>05 61 43 62 43</b></a></p>
                    </div>
                    <div class="email">
                        <p class="black">Email</p>
                        <p><a href="mailto:moijelis@sedrap.fr">moijelis@sedrap.fr</a></p>
                    </div>
                </div>
                <div class="col-12 col-lg-8 form">
                    <?php the_content() ?>
                    <?php
                    if ($return) {
                        if (!$return['error']) {
                            ?>
                            <div role="alert" class="alert alert-success"><?php echo $return['success'] ?></div>
                            <?php
                        } else {
                            ?>
                            <div role="alert" class="alert alert-danger">
                                <ul>
                                    <?php
                                    foreach ($return['errors'] as $error) {
                                        ?>
                                        <li><?php echo $error ?></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <form action="./" method="POST" name="contact">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="row">
                                    <fieldset class="form-group">
                                        <?php
                                        $value = isset($_POST['contact']['nom']) ? $_POST['contact']['nom'] : '';
                                        $class = isset($_POST['contact']) && !$value ? 'is-invalid' : isset($_POST['contact']) ? 'is-valid' : '';
                                        ?>
                                        <input required name="contact[nom]" type="text"
                                               placeholder="Nom*" class="form-control <?php echo $class ?>"
                                               value="<?php echo $value ?>"/>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="row">
                                    <fieldset class="form-group">
                                        <?php
                                        $value = isset($_POST['contact']['prenom']) ? $_POST['contact']['prenom'] : '';
                                        $class = isset($_POST['contact']) && !$value ? 'is-invalid' : isset($_POST['contact']) ? 'is-valid' : '';
                                        ?>
                                        <input required name="contact[prenom]" type="text"
                                               placeholder="Prénom*" class="form-control <?php echo $class ?>"
                                               value="<?php echo $value ?>"/>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="row">
                                    <fieldset class="form-group">
                                        <?php
                                        $value = isset($_POST['contact']['email']) ? $_POST['contact']['email'] : '';
                                        $class = isset($_POST['contact']) && !$value ? 'is-invalid' : isset($_POST['contact']) ? 'is-valid' : '';
                                        ?>
                                        <input required name="contact[email]" type="email"
                                               placeholder="Adresse email*" class="form-control <?php echo $class ?>"
                                               value="<?php echo $value ?>"/>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="row">
                                    <fieldset class="form-group">
                                        <?php
                                        $value = isset($_POST['contact']['reason']) ? $_POST['contact']['reason'] : '';
                                        $class = isset($_POST['contact']) && !$value ? 'is-invalid' : isset($_POST['contact']) ? 'is-valid' : '';
                                        ?>
                                        <select required id="contact_reason" name="contact[reason]"
                                                class="form-control <?php echo $class ?>">
                                            <?php
                                            $reason = array(
                                                'null'                    => 'Objet*',
                                                'Service clients'         => 'Suivi de commande – Réclamation',
                                                'Demande d\'informations' => 'Demande d\'informations',
                                                'Presse/Partenariats'     => 'Presse/Partenariats',
                                                'Autre'                   => 'Autre'
                                            );
                                            foreach ($reason as $value => $name) {
                                                $selected = isset($_POST['contact']['reason']) && $_POST['contact']['reason'] == $value ? 'selected' : '';
                                                ?>
                                                <option <?php echo $selected ?>
                                                        value="<?php echo $value ?>"><?php echo $name ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <fieldset class="form-group">
                                        <?php
                                        $value = isset($_POST['contact']['message']) ? $_POST['contact']['message'] : '';
                                        $class = isset($_POST['contact']) && !$value ? 'is-invalid' : isset($_POST['contact']) ? 'is-valid' : '';
                                        ?>
                                        <textarea rows="10" required name="contact[message]"
                                                  data-required="true" placeholder="Rédigez votre message*"
                                                  class="form-control <?php echo $class ?>"><?php echo $value ?></textarea>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 offset-lg-5">
                                <button type="submit" class="btn orange">Envoyer le message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row faq_cont">
    <div class="col-12 col-lg-8 offset-lg-2">
        <div class="container">
            <div class="row">
                <div class="col-12 align-center faq_title">
                    <span class="info"></span>
                    Foire aux questions
                    <h2>
                        Suivi de commande ?
                        <span>Plus d'informations pour mieux comprendre la livraison gratuite !</span>
                    </h2>
                </div>
            </div>
            <?php
            $questions = get_posts(array(
                'post_type'      => 'question',
                'posts_per_page' => -1,
                'orderby'        => 'menu_order',
                'order'          => 'ASC'
            ));
            foreach ($questions as $question) {
                ?>
                <div class="row question">
                    <div class="col-12">
                        <h3><?php echo $question->post_title ?></h3>
                        <div class="answer">
                            <?php echo wpautop($question->post_content) ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
