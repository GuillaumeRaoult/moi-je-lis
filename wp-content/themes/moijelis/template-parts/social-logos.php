<a href="https://www.facebook.com/editions.sedrap" target="_blank">
    <svg xmlns="http://www.w3.org/2000/svg" id="Groupe_29" data-name="Groupe 29" viewBox="0 0 30 30">
        <defs>
            <style>
                .cls-1{fill:#65b2cf}
            </style>
        </defs>
        <g id="Groupe_27" data-name="Groupe 27">
            <path id="Tracé_74" d="M16.1 22.1V15h2l.3-2.5h-2.3V11c0-.6.4-.7.6-.7h1.6V7.9h-2.2a2.8 2.8 0 0 0-3 3v1.6h-1.4V15h1.4v7.1z" class="cls-1" data-name="Tracé 74"/>
        </g>
        <g id="Groupe_28" data-name="Groupe 28">
            <path id="Tracé_75" d="M15 30a15 15 0 1 1 15-15 14.98 14.98 0 0 1-15 15zm0-28.6A13.6 13.6 0 1 0 28.6 15 13.614 13.614 0 0 0 15 1.4z" class="cls-1" data-name="Tracé 75"/>
        </g>
    </svg>
</a>
<a href="https://www.instagram.com/explore/locations/682635501807576/editions-sedrap/" target="_blank">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
        <defs>
            <style>
                .cls-1{fill:#65b2cf}
            </style>
        </defs>
        <g id="Groupe_34" data-name="Groupe 34" transform="translate(-36.8)">
            <g id="Groupe_32" data-name="Groupe 32">
                <g id="Groupe_31" data-name="Groupe 31">
                    <g id="Groupe_30" data-name="Groupe 30">
                        <path id="Tracé_76" d="M56.2 9.5a1 1 0 1 0 1 1c.1-.6-.4-1-1-1zm-.8-2.8h-7.2a4.8 4.8 0 0 0-4.8 4.8v7a4.8 4.8 0 0 0 4.8 4.8h7.2a4.8 4.8 0 0 0 4.8-4.8v-7a4.8 4.8 0 0 0-4.8-4.8zm-6.6 15.1c-2.7 0-3.9-1.3-3.9-4v-5.7c0-2.9 1.2-4 3.9-4h5.9c2.6 0 3.9 1.2 3.9 4v5.7c0 2.6-1.2 4-3.9 4m-3-11.1A4.3 4.3 0 1 0 56 15a4.268 4.268 0 0 0-4.3-4.3zm0 7.1a2.8 2.8 0 1 1 2.8-2.8 2.8 2.8 0 0 1-2.8 2.8z" class="cls-1" data-name="Tracé 76"/>
                    </g>
                </g>
            </g>
            <g id="Groupe_33" data-name="Groupe 33">
                <path id="Tracé_77" d="M51.8 30a15 15 0 1 1 15-15 14.98 14.98 0 0 1-15 15zm0-28.6A13.6 13.6 0 1 0 65.4 15 13.614 13.614 0 0 0 51.8 1.4z" class="cls-1" data-name="Tracé 77"/>
            </g>
        </g>
    </svg>
</a>
<a href="https://www.youtube.com/channel/UCnqG4KJ9AXAtqlvGBJgkVrw" target="_blank">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
        <defs>
            <style>
                .cls-1{fill:#65b2cf}
            </style>
        </defs>
        <g id="Groupe_38" data-name="Groupe 38" transform="translate(-73.5)">
            <g id="Groupe_35" data-name="Groupe 35">
                <path id="Tracé_78" d="M88.5 30a15 15 0 1 1 15-15 14.98 14.98 0 0 1-15 15zm0-28.6A13.6 13.6 0 1 0 102.2 15 13.765 13.765 0 0 0 88.5 1.4z" class="cls-1" data-name="Tracé 78"/>
            </g>
            <g id="Groupe_37" data-name="Groupe 37">
                <g id="Groupe_36" data-name="Groupe 36">
                    <path id="Tracé_79" d="M96.9 11.4a2.588 2.588 0 0 0-2.5-2.7c-1.8-.1-3.7-.1-5.6-.1h-.6c-1.9 0-3.8 0-5.6.1a2.588 2.588 0 0 0-2.5 2.7c0 1.2-.1 2.4-.1 3.6s0 2.4.1 3.6a2.588 2.588 0 0 0 2.5 2.7c1.9.1 3.9.1 5.9.1s4 0 5.9-.1a2.588 2.588 0 0 0 2.5-2.7c.1-1.2.1-2.4.1-3.6a21.663 21.663 0 0 0-.1-3.6zm-5.8 4.2l-4.7 3c-.5.3-.9.1-.9-.6v-6c0-.6.4-.9.9-.6l4.7 3a.668.668 0 0 1 0 1.2z" class="cls-1" data-name="Tracé 79"/>
                </g>
            </g>
        </g>
    </svg>
</a>