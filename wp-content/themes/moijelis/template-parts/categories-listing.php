<?php
/**
 * Template part for displaying the horizontal list of products categories
 *
 */

?>
<div class="row">
    <div class="col-lg-12">
        <?php
        if (is_tax()) {
            ?>
            <h2>Découvrez les autres niveaux<span>Des livres adaptés au niveau de votre enfant, pour lui donner le goût de la lecture !</span>
            </h2>
            <?php
        }
        ?>
        <?php
        $categories = get_terms([
            'taxonomy'   => 'product_cat',
            'hide_empty' => false,
            'menu_order' => 'ASC'
        ]);
        if ($categories) {
            ?>
            <div class="categories_list clearfix align-center">
                <?php
                foreach ($categories as $category) {
                    if (is_tax()) {
                        if ($category->term_id == get_queried_object()->term_id) {
                            continue;
                        }
                    }
                    $thumbId = get_term_meta($category->term_id, 'thumbnail_id', $single = true);

                    $products = wc_get_products(array(
                        'category' => $category->slug,
                        'limit'    => 1,
                    ));

                    if ($products) {
                        $price = wc_price($products[0]->get_regular_price());
                    }
                    ?>
                    <div class="category_box align-center">
                        <a href="<?php echo get_term_link($category->term_id) ?>">
                            <?php
                            echo wp_get_attachment_image($thumbId, 'full');
                            ?>
                        </a>
                        <span class="title"><?php echo $category->name ?></span>
                        <span class="desc"><?php echo $category->description ?></span>
                        <?php echo $price ?>
                        <a href="<?php echo get_term_link($category->term_id) ?>" class="btn blue">Je découvre</a>
                    </div>
                    <?php
                }
                ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>