<div class="modal fade" id="modalShopInactive" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row content">
                        <div class="col-10 offset-1">
                            <?php echo wpautop(get_option('shop_open_closed_message')) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-lg-6 offset-lg-3">
                            <button type="button" class="btn btn-secondary return" data-dismiss="modal">Continuer la
                                visite
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
