<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Votre article a bien été ajouté</h5>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-8 offset-2">
                            <div class="row align-items-center">
                                <?php
                                if (isset($_POST['product_id']) && $_POST['product_id']) {
                                    $newAddedProductId = $_POST['product_id'];
                                    if ($newAddedProductId) {
                                        $pf      = new WC_Product_Factory();
                                        $product = $pf->get_product($newAddedProductId);
                                        if ($product) {
                                            ?>
                                            <div class="col-12 col-lg-4 offset-lg-1 image">
                                                <?php
                                                echo $product->get_image();
                                                ?>
                                            </div>
                                            <div class="col-12 col-lg-7 content">
                                                <p class="product_title"><?php echo $product->get_name(); ?></p>
                                                <p class="product_cat"><?php echo wc_get_product_category_list($product->get_id(), ', ', '<span class="posted_in">' . ' ', '</span>'); ?></p>
                                                <p class="product_tag"><span
                                                            class="tagged_as"><?php echo getProductCatDescription($product->get_id()) ?></span>
                                                </p>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <button type="button" class="btn btn-secondary return" data-dismiss="modal">Continuer
                            mes achats
                        </button>
                    </div>
                    <div class="col-12 col-lg-6">
                        <a href="<?php echo wc_get_cart_url() ?>" class="btn orange">Finaliser ma commande</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
