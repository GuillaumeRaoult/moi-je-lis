<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Moi_je_lis
 */

?>
<div class="row">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php
        if (is_cart() || is_checkout()) {
            get_template_part('template-parts/cart', 'breadcrumb');
        }

        if (is_checkout()) {
            ?>
            <a class="d-block d-lg-none see_order_review" href="#order_review_heading">Voir le récapitulatif de la commande</a>
            <?php
        }
        ?>
        <header class="entry-header">
            <?php the_title('<h1 class="entry-title single">', '</h1>'); ?>
        </header>

        <?php moijelis_post_thumbnail(); ?>

        <div class="entry-content">
            <?php
            the_content();

            wp_link_pages(array(
                'before' => '<div class="page-links">' . esc_html__('Pages:', 'moijelis'),
                'after'  => '</div>',
            ));
            ?>
        </div>
    </article>
    <?php
    if (is_checkout()) {
        ?>
        <div class="col-12 col-lg-4 sidebar_order_review">
            <h3 id="order_review_heading"><?php _e('Récapitulatif de commande', 'woocommerce'); ?></h3>
            <?php
            if (is_checkout() && !is_order_received_page()) {
                do_action('woocommerce_checkout_before_order_review');
                ?>
                <div id="order_review" class="woocommerce-checkout-review-order">
                    <?php do_action('woocommerce_checkout_order_review'); ?>
                </div>
                <?php
                do_action('woocommerce_checkout_after_order_review');
            } else if (is_order_received_page()) {
                $order_id  = apply_filters('woocommerce_thankyou_order_id', absint($wp->query_vars['order-received']));
                $order_key = apply_filters('woocommerce_thankyou_order_key', empty($_GET['key']) ? '' : wc_clean(wp_unslash($_GET['key']))); // WPCS: input var ok, CSRF ok.
                if ($order_id > 0) {
                    $order = wc_get_order($order_id);
                    if (!$order || $order->get_order_key() !== $order_key) {
                        $order = false;
                    }
                }

                if ($order) {
                    ?>
                    <div id="order_review" class="woocommerce-checkout-review-order">
                        <?php
                        do_action('woocommerce_thankyou', $order->get_id());
                        ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <?php
    }
    ?>
</div>
