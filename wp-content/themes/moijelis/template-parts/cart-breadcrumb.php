<ul class="shopping_breadcrumb">
    <li class="<?php echo is_cart() ? 'active' : '' ?>">
        <a href="<?php echo wc_get_cart_url() ?>">1. Panier</a><span>></span>
    </li>
    <li class="<?php echo is_checkout() && !is_checkout_pay_page() && !is_order_received_page() ? 'active' : '' ?>">
        <?php
        if (is_checkout() && !is_order_received_page()) {
            ?>
            <a href="<?php wc_get_checkout_url() ?>">
            <?php
        }
        ?>
        2. Commande
        <?php
        if (is_checkout() && !is_order_received_page()) {
        ?>
            </a>
        <?php
        }
        ?>
        <span>></span>
    </li>
    <li class="<?php echo is_checkout_pay_page() ? 'active' : '' ?>">
        3. Paiement
        <span>></span>
    </li>
    <li class="<?php echo is_order_received_page() ? 'active' : '' ?>">
        4. Confirmation
    </li>
</ul>