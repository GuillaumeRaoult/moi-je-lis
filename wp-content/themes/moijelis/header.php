<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Moi_je_lis
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link href="<?php echo get_template_directory_uri() ?>/assets/images/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site container-fluid">
    <header id="masthead" class="site-header">
        <div class="row site-branding">
            <div class="container">
                <div class="row">
                    <div class="col-4 d-none d-lg-block logo_sedrap">
                        <?php
                        if (!is_cart() && !is_checkout()) {
                            ?>
                            <a href="http://www.sedrap.fr" target="_blank">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo-sedrap-noir.png"
                                     srcset="<?php echo get_template_directory_uri() ?>/assets/images/logo-sedrap-noir@2x.png 2x,
                        <?php echo get_template_directory_uri() ?>/assets/images/logo-sedrap-noir@3x.png 3x"
                                     alt="Editions Sedrap"/>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-12 col-lg-4 align-center logo_moijelis">
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo-moi-jelis.png"
                                 srcset="<?php echo get_template_directory_uri() ?>/assets/images/logo-moi-jelis@2x.png 2x, <?php echo get_template_directory_uri() ?>/assets/images/logo-moi-jelis@3x.png 3x"
                                 alt="Moi je lis"/>
                        </a>
                    </div>
                    <?php
                    if (!is_cart() && !is_checkout()) {
                        ?>
                        <div class="col-4 d-none d-lg-block align-right social">
                            <?php
                            get_template_part('template-parts/social', 'logos');
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <nav id="site-navigation" class="row main-navigation">
            <div class="container relative">
                <div class="row">
                    <?php
                    if (is_cart() || is_order_received_page()) {
                        ?>
                        <div class="col-12 col-lg-3 continue_shopping">
                            <a href="<?php echo get_home_url('/') ?>">Continuer mes achats</a>
                        </div>
                        <?php
                    } else if (is_checkout() && !is_order_received_page()) {
                        ?>
                        <div class="col-12 col-lg-3 continue_shopping">
                            <a href="<?php echo wc_get_cart_url() ?>">Revenir au panier</a>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-7 offset-1">
                            <button class="menu-toggle" aria-controls="primary-menu"
                                    aria-expanded="false"><span
                                        class="dashicons dashicons-menu"></span><?php esc_html_e('Menu', 'moijelis'); ?>
                            </button>
                        </div>
                        <div class="col-11 menu_cont align-center">
                            <?php
                            wp_nav_menu(array(
                                'theme_location' => 'Header',
                                'before'         => '<span class="sep">•</span>'
                            ));
                            ?>
                        </div>
                        <?php
                            if (is_user_logged_in() || isShopOpen()) {
                                ?>
                                <div class="col-lg-1 col-3 align-right cart_icon">
                                </div>
                                <?php
                            }
                    }
                    ?>
                </div>
            </div>
        </nav>
    </header>
    <div id="content" class="site-content">
