<?php
/**
 * Show error messages
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/notices/error.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

if (!$messages) {
    return;
}

?>
<div class="row">
    <?php
    if (is_cart() || is_checkout()) {
        $class = 'col-12';
    } else {
        $class = 'col-8 offset-2';
    }
    ?>
    <div class="<?php echo $class ?>">
        <ul class="woocommerce-error alert alert-danger" role="alert">
            <?php
            foreach ($messages as $message) {
                $class = count($messages) == 1 ? 'alone' : '';
                ?>
                <li class="<?php echo $class ?>"><?php echo wp_kses_post($message); ?></li>
                <?php
            }
            ?>
        </ul>
    </div>
</div>