<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}
?>
<li <?php wc_product_class(); ?>>
    <div class="row">
        <div class="col-12">
            <?php
            /**
             * Hook: woocommerce_before_shop_loop_item.
             *
             * @hooked woocommerce_template_loop_product_link_open - 10
             */
            do_action('woocommerce_before_shop_loop_item');

            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-8 product_infos_left">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <?php
                    /**
                     * Image
                     *
                     * Hook: woocommerce_before_shop_loop_item_title.
                     *
                     * @hooked woocommerce_show_product_loop_sale_flash - 10
                     * @hooked woocommerce_template_loop_product_thumbnail - 10
                     */
                    do_action('woocommerce_before_shop_loop_item_title');
                    ?>
                </div>
                <div class="col-12 col-lg-8">
                    <?php
                    /**
                     * Title
                     *
                     * Hook: woocommerce_shop_loop_item_title.
                     *
                     * @hooked woocommerce_template_loop_product_title - 10
                     */
                    do_action('woocommerce_shop_loop_item_title');

                    ?>
                    <div class="woocommerce-loop-product__author">
                        <?php
                        echo apply_filters('woocommerce_short_description', $post->post_excerpt);
                        ?>
                    </div>
                    <div class="woocommerce-loop-product__description">
                        <?php
                        the_content();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4 product_infos_right align-center">
            <div class="woocommerce-loop-product__cat">
                <?php
                echo wc_get_product_category_list($product->get_id(), ', ', '<span class="posted_in">' . ' ', '</span>');
                ?>
            </div>
            <div class="woocommerce-loop-product__tag">
                <?php
                $terms = get_the_terms( $product->get_id(), 'product_cat' );
                if ($terms) {
                    $term = $terms[0];
                    ?>
                    <span class="tagged_as"><?php echo $term->description ?></span>
                    <?php
                }
                ?>
            </div>
            <?php
            /**
             * Price
             *
             * Hook: woocommerce_after_shop_loop_item_title.
             *
             * @hooked woocommerce_template_loop_rating - 5
             * @hooked woocommerce_template_loop_price - 10
             */
            do_action('woocommerce_after_shop_loop_item_title');

            woocommerce_quantity_input( array(
                'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
                'product_id' => get_the_ID()
            ) );

            /**
             * Add cart
             *
             * Hook: woocommerce_after_shop_loop_item.
             *
             * @hooked woocommerce_template_loop_product_link_close - 5
             * @hooked woocommerce_template_loop_add_to_cart - 10
             */
            do_action('woocommerce_after_shop_loop_item');
            ?>
        </div>
    </div>
</li>
