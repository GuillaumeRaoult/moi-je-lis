<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load colors.
$bg              = get_option( 'woocommerce_email_background_color' );
$body            = get_option( 'woocommerce_email_body_background_color' );
$base            = get_option( 'woocommerce_email_base_color' );
$base_text       = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text            = get_option( 'woocommerce_email_text_color' );

// Pick a contrasting color for links.
$link = wc_hex_is_light( $base ) ? $base : $base_text;
if ( wc_hex_is_light( $body ) ) {
	$link = wc_hex_is_light( $base ) ? $base_text : $base;
}

$bg_darker_10    = wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
?>
#wrapper {
	background-color: <?php echo esc_attr( $bg ); ?>;
	margin: 0;
	padding: 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
}

#template_container {
	background-color: #fff;
	border: 1px solid <?php echo esc_attr( $bg_darker_10 ); ?>;
	border-radius: 3px !important;
}

#template_header_image {
    background-color: <?php echo esc_attr( $base ); ?>;
}

#template_header_image img {
    position: relative;
    top: 27px;
}

#template_header_bg {
    background: #10779e;
    height: 68px;
}

#template_header {
	background-color: #fff;
	border-radius: 3px 3px 0 0 !important;
	color: <?php echo esc_attr( $base_text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_header h1,
#template_header h1 a {
	color: #707070;
}

#template_help {
    background: #10779e;
}

#template_footer {
    background: #004c64;
}

#template_footer img {
    border: 0;
}

#template_footer .social {
    padding-right: 20px;
}

#template_footer .social a {
    display: inline-block;
}

#template_footer .social a svg {
    width: 30px;
    height: 30px;
    display: block;
}

#template_footer .social a svg path {
    fill: #fff !important;
}

#template_help td, #template_footer.td {
	padding: 0;
	-webkit-border-radius: 6px;
}

#template_help #credit {
	border:0;
    font-size: 18px;
    line-height: 1.44;
    color: #65b2cf;
	font-family: Arial;
	text-align:center;
    padding: 20px 0;
}

#template_help #credit h3 {
    font-weight: bold;
    color: #ffffff;
    text-align: center;
}

#template_help #credit a {
    color: #ffffff;
}

#body_content {
	background-color: <?php echo esc_attr( $body ); ?>;
}

#body_content table td {
	padding: 48px 48px 0;
}

#body_content table td td {
	padding: 0;
}

#body_content table td th {
	padding: 0;
}

#body_content td ul.wc-item-meta {
	font-size: small;
	margin: 1em 0 0;
	padding: 0;
	list-style: none;
}

#body_content td ul.wc-item-meta li {
	margin: 0.5em 0 0;
	padding: 0;
}

#body_content td ul.wc-item-meta li p {
	margin: 0;
}

#body_content p {
	margin: 0 0 16px;
}

#body_content .item_details p {
    color: #707070;
    margin: 0;
    font-size: 20px;
    line-height: 1.2;
    text-align: left;
}

#body_content .item_details a {
    color: #707070;
    text-decoration: none;
}

#body_content .item_details p.product_title {
    font-size: 24px;
    font-weight: bold;
    line-height: 1;
}

#body_content tfoot hr {
    background: #dfdfdf;
    height: 1px;
    border: 0;
}

#body_content tfoot .td {
    padding: 5px 0;
}

#body_content_inner {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 14px;
	line-height: 150%;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

#body_content_inner img {
    border: solid 6px rgba(112, 112, 112, 0.2);
    margin-bottom: 10px;
}

#body_content_inner .total .td {
    color: #f39200;
    font-size: 24px;
    line-height: 1.21;
    font-weight: normal;
}

#body_content_inner .total .amount {
    font-size: 30px;
    line-height: 1.23;
}

#body_content_inner .total .includes_tax {
    display: none;
}

.td {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	border: none;
	vertical-align: middle;
}

.address {
	color: #f39200;
    margin-bottom: 30px;
    font-size: 18px;
    font-weight: 300;
    line-height: 1.17;
    font-style: initial;
}

.shipping {
    color: #f39200;
    font-size: 20px;
    font-weight: 300;
    line-height: 1.2;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

.link {
	color: <?php echo esc_attr( $base ); ?>;
}

#header_wrapper {
	padding: 36px 48px;
	display: block;
}

h1 {
	color: #707070;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
    font-size: 18px;
    font-weight: 300;
    line-height: 1.22;
	margin: 0;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h1 span {
    display: block;
    font-size: 30px;
    line-height: 0.73;
}

h2 {
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	margin: 0 0 20px;
    padding-bottom: 18px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
    font-size: 24px;
    font-weight: 300;
    line-height: 1.21;
    text-align: left;
    color: #707070;
    border-bottom: solid 1px #dfdfdf;
}

h3 {
	color: <?php echo esc_attr( $base ); ?>;
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 16px;
	font-weight: bold;
	line-height: 130%;
	margin: 16px 0 8px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

a {
	color: <?php echo esc_attr( $link ); ?>;
	font-weight: normal;
	text-decoration: underline;
}

img {
	border: none;
	display: inline-block;
	font-size: 14px;
	font-weight: bold;
	height: auto;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
	vertical-align: middle;
	margin-<?php echo is_rtl() ? 'left' : 'right'; ?>: 10px;
}

<?php
