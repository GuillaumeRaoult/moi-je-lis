<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates/Emails
 * @version       2.3.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

?>
</div>
</td>
</tr>
</table>
<!-- End Content -->
</td>
</tr>
</table>
<!-- End Body -->
</td>
</tr>
<tr>
    <td align="center" valign="top">
        <!-- Footer -->
        <table border="0" cellpadding="10" cellspacing="0" width="100%" id="template_help">
            <tr>
                <td valign="top" align="center">
                    <table border="0" cellpadding="10" cellspacing="0" width="600">
                        <tr>
                            <td colspan="2" valign="middle" id="credit">
                                <h3>Pour toute question</h3>
                                <p>
                                    Contactez les Edition Sedrap à cette adresse : <a href="mailto:moijelis@sedrap.fr">moijelis@sedrap.fr</a><br/>
                                    Edition SEDRAP • 9, Rue des Frères Boudé, 31106 Toulouse
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- End Footer -->
    </td>
</tr>
<tr>
    <td align="center" valign="top">
        <!-- Footer -->
        <table border="0" cellpadding="10" cellspacing="0" width="100%" id="template_footer">
            <tr>
                <td valign="top" align="center">
                    <table border="0" cellpadding="10" cellspacing="0" width="600">
                        <tr>
                            <td align="left">
                                <a target="_blank" href="http://www.sedrap.fr">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo-sedrap-noir.png"
                                         srcset="<?php echo get_template_directory_uri() ?>/assets/images/logo-sedrap-noir@2x.png 2x,
                                        <?php echo get_template_directory_uri() ?>/assets/images/logo-sedrap-noir@3x.png 3x"
                                         alt="Editions Sedrap"/>
                                </a>
                            </td>
                            <td align="right" class="social">
                                <?php
                                get_template_part('template-parts/social', 'logos');
                                ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!-- End Footer -->
    </td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</body>
</html>
