<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

?>

    <div class="row">
        <div class="col-12">
            <?php
            if (!is_checkout_pay_page()) {
                do_action('woocommerce_review_order_before_cart_contents');
                foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                    $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key)) {
                        ?>
                        <div class="row align-items-center product_desc">
                            <div class="col-4 product-thumbnail">
                                <?php
                                echo $_product->get_image();
                                ?>
                            </div>
                            <div class="col-8">
                                <p class="product_title"><?php echo apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;'; ?></p>
                                <p class="product_cat"><?php echo wc_get_product_category_list($_product->get_id(), ', ', '<span class="posted_in">' . ' ', '</span>'); ?></p>
                                <p class="product_tag"><span
                                            class="tagged_as"><?php echo getProductCatDescription($_product->get_id()) ?></span>
                                </p>
                                <p class="product_quantity"><?php echo apply_filters('woocommerce_checkout_cart_item_quantity', sprintf('%s ex.', $cart_item['quantity']), $cart_item, $cart_item_key); ?></p>
                                <?php echo wc_get_formatted_cart_item_data($cart_item); ?>
                                <p class="product_price"><?php echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); ?></p>
                            </div>
                        </div>
                        <?php
                    }
                }
                do_action('woocommerce_review_order_after_cart_contents');
                ?>
                <div class="row cart_amount">
                    <div class="col-12 cart_subtotals">
                        <hr/>
                        <div class="row align-items-center">
                            <div class="col-6">
                                Total H.T.
                            </div>
                            <div class="col-6 align-right">
                                <?php echo WC()->cart->get_total_ex_tax() ?>
                            </div>
                        </div>
                    </div>
                    <?php if (wc_tax_enabled() && !WC()->cart->display_prices_including_tax()) : ?>
                        <div class="col-12 cart_tva">
                            <?php if ('itemized' === get_option('woocommerce_tax_total_display')) : ?>
                                <?php foreach (WC()->cart->get_tax_totals() as $code => $tax) : ?>
                                    <div class="row align-items-center tax-rate tax-rate-<?php echo sanitize_title($code); ?>">
                                        <div class="col-6">
                                            <?php echo esc_html($tax->label); ?>
                                        </div>
                                        <div class="col-6 align-right">
                                            <?php echo wp_kses_post($tax->formatted_amount); ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <div class="row align-items-center">
                                    <div class="col-6">
                                        <?php echo esc_html(WC()->countries->tax_or_vat()); ?>
                                    </div>
                                    <div class="col-6 align-right">
                                        <?php wc_cart_totals_taxes_total_html(); ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <?php foreach (WC()->cart->get_coupons() as $code => $coupon) : ?>
                        <div class="col-12 cart_coupon">
                            <div class="row align-items-center">
                                <div class="cart-discount coupon-<?php echo esc_attr(sanitize_title($code)); ?>">
                                    <div class="col-6">
                                        <?php wc_cart_totals_coupon_label($coupon); ?>
                                    </div>
                                    <div class="col-6 align-right">
                                        <?php wc_cart_totals_coupon_html($coupon); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php if (WC()->cart->needs_shipping() && WC()->cart->show_shipping()) : ?>
                        <?php do_action('woocommerce_review_order_before_shipping'); ?>
                        <div class="col-12 cart_shipping">
                            <?php wc_cart_totals_shipping_html(); ?>
                        </div>
                        <?php do_action('woocommerce_review_order_after_shipping'); ?>
                    <?php endif; ?>
                    <?php foreach (WC()->cart->get_fees() as $fee) : ?>
                        <div class="col-12 cart_fees">
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <?php echo esc_html($fee->name); ?>
                                </div>
                                <div class="col-6 align-right">
                                    <?php wc_cart_totals_fee_html($fee); ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="col-12 cart_total">
                        <hr/>
                        <?php do_action('woocommerce_review_order_before_order_total'); ?>
                        <div class="row align-items-center">
                            <div class="col-6">
                                <?php _e('Total T.T.C.', 'woocommerce'); ?>
                            </div>
                            <div class="col-6 align-right">
                                <?php wc_cart_totals_order_total_html(); ?>
                            </div>
                        </div>
                        <?php do_action('woocommerce_review_order_after_order_total'); ?>
                    </div>
                </div>
                <?php
            } else {
                global $wp;
                $order = wc_get_order($wp->query_vars['order-pay']);
                foreach ($order->get_items() as $item_id => $item) {
                    $productId   = $item->get_product_id();
                    $pf          = new WC_Product_Factory();
                    $_product    = $pf->get_product($productId);
                    $tax_display = get_option('woocommerce_tax_display_cart');
                    ?>
                    <div class="row align-items-center product_desc">
                        <div class="col-4 product-thumbnail">
                            <?php
                            echo $_product->get_image();
                            ?>
                        </div>
                        <div class="col-8">
                            <p class="product_title"><?php echo apply_filters('woocommerce_order_item_name', esc_html($item->get_name()), $item, false); ?></p>
                            <p class="product_cat"><?php echo wc_get_product_category_list($_product->get_id(), ', ', '<span class="posted_in">' . ' ', '</span>'); ?></p>
                            <p class="product_tag"><?php echo getProductCatDescription($_product->get_id()) ?></p>
                            <p class="product_quantity"><?php echo apply_filters('woocommerce_order_item_quantity_html', sprintf('%s ex.', esc_html($item->get_quantity())), $item); ?></p>
                            <p class="product_price"><?php echo $order->get_formatted_line_subtotal($item); ?></p>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="row cart_amount">
                    <div class="col-12 cart_subtotals">
                        <hr/>
                        <div class="row align-items-center">
                            <div class="col-6">
                                Total H.T.
                            </div>
                            <div class="col-6 align-right">
                                <?php
                                echo $order->get_subtotal_to_display(false, $tax_display)
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 cart_shipping">
                        <div class="row">
                            <div class="col-6">
                                <?php echo $order->get_shipping_to_display($tax_display); ?>
                            </div>
                            <div class="col-6 align-right">
                                <?php
                                echo wc_price($order->get_shipping_total(), array('currency' => $order->get_currency()))
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 cart_total">
                        <hr/>
                        <div class="row align-items-center">
                            <div class="col-6">
                                <?php _e('Total T.T.C.', 'woocommerce'); ?>
                            </div>
                            <div class="col-6 align-right">
                                <?php
                                echo $order->get_formatted_order_total($tax_display);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
<?php

if (is_checkout_pay_page()) {
    global $wp;
    $order = wc_get_order($wp->query_vars['order-pay']);
    ?>
    <div class="row">
        <div class="col-12 school_recap">
            <h3>Adresse de livraison<a href="javascript:void(0)" class="tooltip_icon" data-toggle="tooltip"
                                       title="Votre commande sera livrée à partir de la rentrée des vacances de la Toussaint, et remise à votre enfant."></a></h3>
            <p class="name"><?php echo $order->get_shipping_address_1() ?></p>
            <p class="address"><?php echo $order->get_shipping_address_2() ?></p>
            <p class="city"><span class="cp"><?php echo $order->get_shipping_postcode() ?></span> <span
                        class="ville"><?php echo $order->get_shipping_city() ?></span></p>
        </div>
    </div>
    <?php
}
