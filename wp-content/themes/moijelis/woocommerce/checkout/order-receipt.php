<?php
/**
 * Checkout Order Receipt Template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/order-receipt.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.2.0
 */

if (!defined('ABSPATH')) {
    exit;
}

//We will manually call the iframe for Payzen
if ($order->get_payment_method() != 'payzenstd') {
    do_action('woocommerce_receipt_' . $order->get_payment_method(), $order->get_id());
} else {
    global $woocommerce;

    $order = new WC_Order($order->get_id());
    $link = add_query_arg('wc-api', 'WC_Gateway_PayzenStd', home_url('/'));
    ?>
    <iframe name="payzen-iframe" class="payzen-iframe" id="payzen_iframe" src="<?php echo $link ?>" >
    </iframe>
    <?php
}
?>

<div class="clear"></div>
