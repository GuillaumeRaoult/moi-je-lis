<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

get_header('shop');

?>
    <div class="row">
        <div class="col-12">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php
                        /**
                         * Hook: woocommerce_before_main_content.
                         *
                         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                         * @hooked woocommerce_breadcrumb - 20
                         * @hooked WC_Structured_Data::generate_website_data() - 30
                         */
                        do_action('woocommerce_before_main_content');
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <header class="woocommerce-products-header align-center">
                            <?php
                            if (apply_filters('woocommerce_show_page_title', true)) {
                                ?>
                                <?php woocommerce_page_title(); ?>
                                <h1 class="woocommerce-products-header__title page-title">
                                    <?php
                                    echo get_term_meta(get_queried_object_id(), 'moijelis_term_category_page_title', true);
                                    ?>
                                    <span><?php echo get_term_meta(get_queried_object_id(), 'moijelis_term_category_page_description', true) ?></span>
                                </h1>
                                <?php
                            }
                            ?>
                        </header>
                    </div>
                </div>
            </div>
            <div class="container no-padding-mobile">
                <div class="row">
                    <div class="col-12 no-padding-mobile">
                        <?php
                        if (woocommerce_product_loop()) {

                            /**
                             * Hook: woocommerce_before_shop_loop.
                             *
                             * @hooked wc_print_notices - 10
                             * @hooked woocommerce_result_count - 20
                             * @hooked woocommerce_catalog_ordering - 30
                             */
                            do_action('woocommerce_before_shop_loop');

                            woocommerce_product_loop_start();

                            if (wc_get_loop_prop('total')) {
                                while (have_posts()) {
                                    the_post();

                                    /**
                                     * Hook: woocommerce_shop_loop.
                                     *
                                     * @hooked WC_Structured_Data::generate_product_data() - 10
                                     */
                                    do_action('woocommerce_shop_loop');

                                    wc_get_template_part('content', 'product');
                                }
                            }

                            woocommerce_product_loop_end();

                            /**
                             * Hook: woocommerce_after_shop_loop.
                             *
                             * @hooked woocommerce_pagination - 10
                             */
                            do_action('woocommerce_after_shop_loop');
                        } else {
                            /**
                             * Hook: woocommerce_no_products_found.
                             *
                             * @hooked wc_no_products_found - 10
                             */
                            do_action('woocommerce_no_products_found');
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php
                        /**
                         * Hook: woocommerce_after_main_content.
                         *
                         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                         */
                        do_action('woocommerce_after_main_content');
                        ?>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php
                        $desc = get_term_meta(get_queried_object_id(), 'moijelis_term_category_page_description_bottom', true);
                        if ($desc) {
                            ?>
                            <div class="row">
                                <div class="col-12 col-lg-8 offset-lg-2 cat_description_bottom">
                                    <?php
                                    echo $desc;
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                        get_template_part('template-parts/categories', 'listing');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer('shop');