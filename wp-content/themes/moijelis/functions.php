<?php
/**
 * Moi je lis functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Moi_je_lis
 */

if (!function_exists('moijelis_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function moijelis_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Moi je lis, use a find and replace
         * to change 'moijelis' to the name of your theme in all the template files.
         */
        load_theme_textdomain('moijelis', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'Header' => esc_html__('Header', 'moijelis'),
            'Footer' => esc_html__('Footer', 'moijelis'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('moijelis_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif;
add_action('after_setup_theme', 'moijelis_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function moijelis_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('moijelis_content_width', 640);
}

add_action('after_setup_theme', 'moijelis_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function moijelis_widgets_init()
{
    register_sidebar(array(
        'name'          => esc_html__('Sidebar', 'moijelis'),
        'id'            => 'sidebar-1',
        'description'   => esc_html__('Add widgets here.', 'moijelis'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ));
}

add_action('widgets_init', 'moijelis_widgets_init');


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load WooCommerce compatibility file.
 */
if (class_exists('WooCommerce')) {
    require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * CPT
 */
require get_template_directory() . '/inc/content-types.php';

/**
 * Custom CMB2 metas boxes
 */
require get_template_directory() . '/inc/metas.php';

/**
 * AJAX functions
 */
require get_template_directory() . '/inc/ajax.php';

/**
 * Global hooks for moijelis
 */
require get_template_directory() . '/inc/moijelis.php';

/**
 * Shop inactive management
 */
require get_template_directory() . '/inc/shop-open.php';

/**
 * Enqueue scripts and styles.
 */
function moijelis_scripts()
{
    wp_enqueue_style('select2');
    wp_enqueue_style('dashicons');
    wp_enqueue_script('popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), '1.14.3', true);
    wp_enqueue_style('bootstrap-css', '//stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css', array(), '4.1.1');
    wp_enqueue_script('touch-swipe', '//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js', array('jquery'), '1.6.4', true);
    wp_enqueue_script('bootstrap-js', '//stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array('jquery'), '4.1.1', true);
    wp_enqueue_style('moijelis-style', get_stylesheet_uri());
    wp_enqueue_style('moijelis-custom-style', get_theme_file_uri('/assets/css/moijelis.min.css'), array(), filemtime(get_stylesheet_directory() . '/assets/css/moijelis.min.css'));

    wp_enqueue_style('montserrat', '//fonts.googleapis.com/css?family=Montserrat:300,400,600,700');
    wp_enqueue_script('moijelis', get_theme_file_uri('/assets/js/moijelis.js'), array('jquery'), filemtime(get_stylesheet_directory() . '/assets/js/moijelis.js'), true);
    wp_localize_script('moijelis', 'wpscript', array(
        'ajaxurl' => admin_url('admin-ajax.php')
    ));

    wp_enqueue_script('selectWoo-fr', '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/fr.js', array('selectWoo'), '1.0.0');

    wp_enqueue_script('moijelis-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);

    wp_enqueue_script('moijelis-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'moijelis_scripts');

function disable_wp_emojicons()
{

    // all actions related to emojis
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');

    // filter to remove TinyMCE emojis
    add_filter('tiny_mce_plugins', 'disable_emojicons_tinymce');
}

add_action('init', 'disable_wp_emojicons');

function disable_emojicons_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

function getProductCatDescription($productId)
{
    $terms = wc_get_product_terms($productId, 'product_cat');
    $desc  = '';
    if ($terms) {
        $desc = $terms[0]->description;
    }

    return $desc;
}

add_action('wp_head', 'analyticsCode', 20);
function analyticsCode()
{
    if (!WP_DEBUG) {
        ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-124442440-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', 'UA-124442440-1');
        </script>
        <?php
    }
}

add_action('phpmailer_init', 'sedrap_smtp');
function sedrap_smtp($phpmailer)
{
    $phpmailer->SMTPOptions = array(
        'ssl' => array(
            'verify_peer'       => false,
            'verify_peer_name'  => false,
            'allow_self_signed' => true
        )
    );
}
