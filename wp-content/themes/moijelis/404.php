<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link    https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Moi_je_lis
 */

get_header();
?>
    <div class="row">
        <div class="col-12">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <section class="error-404 not-found">
                        <header class="page-header">
                            <h1 class="page-title">Page non trouvée</h1>
                        </header>
                        <div class="page-content align-center">
                            <p>Il semblerait qu'aucun produit ne corresponde à votre recherche</p>
                        </div>
                        <?php
                        get_template_part('template-parts/categories', 'listing');
                        ?>
                    </section>
                </main>
            </div>
        </div>
    </div>
<?php
get_footer();
